## OpenSubtitles API

**Simple API to interact with opensubtitles services**

    var openSubtitles = require('popcorn-opensubtitles');
    
    var userAgent = 'User Agent XX';
    
    var queryParams = {
        imdbid: "tt0903747",
        episode: "1",
        season: "5",
        filename: "Breaking.Bad.S05E01.Live.Free.or.Die.HDTV.x264-FQM"
    };
    
    openSubtitles.searchEpisode(queryParams, userAgent)
        .then(function(result) {
            console.log(result);
        }).fail(function(error) {
            console.log(error);
        });

- Based on OpenSRTJS, under MIT - Copyright (c) 2014 Eóin Martin
- This code is registered under MIT - Copyright (c) 2015

### The MIT License (MIT)

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.
